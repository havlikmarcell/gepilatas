
2
	x_to_feedPlaceholder*
dtype0*
shape: 
4
PlaceholderPlaceholder*
dtype0*
shape: 
2
	keep_probPlaceholder*
dtype0*
shape: 
P
random_normal/shapeConst*
dtype0*%
valueB"         @   
?
random_normal/meanConst*
dtype0*
valueB
 *    
A
random_normal/stddevConst*
dtype0*
valueB
 *  ?
~
"random_normal/RandomStandardNormalRandomStandardNormalrandom_normal/shape*
dtype0*
seed2 *

seed *
T0
[
random_normal/mulMul"random_normal/RandomStandardNormalrandom_normal/stddev*
T0
D
random_normalAddrandom_normal/mulrandom_normal/mean*
T0
b
VariableVariable*
dtype0*
shape:@*
	container *
shared_name 
d
Variable/AssignAssignVariablerandom_normal*
validate_shape(*
use_locking(*
T0
,
Variable/readIdentityVariable*
T0
R
random_normal_1/shapeConst*
dtype0*%
valueB"      @      
A
random_normal_1/meanConst*
dtype0*
valueB
 *    
C
random_normal_1/stddevConst*
dtype0*
valueB
 *  ?

$random_normal_1/RandomStandardNormalRandomStandardNormalrandom_normal_1/shape*
dtype0*
seed2 *

seed *
T0
a
random_normal_1/mulMul$random_normal_1/RandomStandardNormalrandom_normal_1/stddev*
T0
J
random_normal_1Addrandom_normal_1/mulrandom_normal_1/mean*
T0
e

Variable_1Variable*
dtype0*
shape:@*
	container *
shared_name 
j
Variable_1/AssignAssign
Variable_1random_normal_1*
validate_shape(*
use_locking(*
T0
0
Variable_1/readIdentity
Variable_1*
T0
R
random_normal_2/shapeConst*
dtype0*%
valueB"            
A
random_normal_2/meanConst*
dtype0*
valueB
 *    
C
random_normal_2/stddevConst*
dtype0*
valueB
 *  ?

$random_normal_2/RandomStandardNormalRandomStandardNormalrandom_normal_2/shape*
dtype0*
seed2 *

seed *
T0
a
random_normal_2/mulMul$random_normal_2/RandomStandardNormalrandom_normal_2/stddev*
T0
J
random_normal_2Addrandom_normal_2/mulrandom_normal_2/mean*
T0
f

Variable_2Variable*
dtype0*
shape:*
	container *
shared_name 
j
Variable_2/AssignAssign
Variable_2random_normal_2*
validate_shape(*
use_locking(*
T0
0
Variable_2/readIdentity
Variable_2*
T0
J
random_normal_3/shapeConst*
dtype0*
valueB"      
A
random_normal_3/meanConst*
dtype0*
valueB
 *    
C
random_normal_3/stddevConst*
dtype0*
valueB
 *  ?

$random_normal_3/RandomStandardNormalRandomStandardNormalrandom_normal_3/shape*
dtype0*
seed2 *

seed *
T0
a
random_normal_3/mulMul$random_normal_3/RandomStandardNormalrandom_normal_3/stddev*
T0
J
random_normal_3Addrandom_normal_3/mulrandom_normal_3/mean*
T0
^

Variable_3Variable*
dtype0*
shape:
 *
	container *
shared_name 
j
Variable_3/AssignAssign
Variable_3random_normal_3*
validate_shape(*
use_locking(*
T0
0
Variable_3/readIdentity
Variable_3*
T0
J
random_normal_4/shapeConst*
dtype0*
valueB"      
A
random_normal_4/meanConst*
dtype0*
valueB
 *    
C
random_normal_4/stddevConst*
dtype0*
valueB
 *  ?

$random_normal_4/RandomStandardNormalRandomStandardNormalrandom_normal_4/shape*
dtype0*
seed2 *

seed *
T0
a
random_normal_4/mulMul$random_normal_4/RandomStandardNormalrandom_normal_4/stddev*
T0
J
random_normal_4Addrandom_normal_4/mulrandom_normal_4/mean*
T0
^

Variable_4Variable*
dtype0*
shape:
*
	container *
shared_name 
j
Variable_4/AssignAssign
Variable_4random_normal_4*
validate_shape(*
use_locking(*
T0
0
Variable_4/readIdentity
Variable_4*
T0
J
random_normal_5/shapeConst*
dtype0*
valueB"      
A
random_normal_5/meanConst*
dtype0*
valueB
 *    
C
random_normal_5/stddevConst*
dtype0*
valueB
 *  ?

$random_normal_5/RandomStandardNormalRandomStandardNormalrandom_normal_5/shape*
dtype0*
seed2 *

seed *
T0
a
random_normal_5/mulMul$random_normal_5/RandomStandardNormalrandom_normal_5/stddev*
T0
J
random_normal_5Addrandom_normal_5/mulrandom_normal_5/mean*
T0
]

Variable_5Variable*
dtype0*
shape:	*
	container *
shared_name 
j
Variable_5/AssignAssign
Variable_5random_normal_5*
validate_shape(*
use_locking(*
T0
0
Variable_5/readIdentity
Variable_5*
T0
C
random_normal_6/shapeConst*
dtype0*
valueB:@
A
random_normal_6/meanConst*
dtype0*
valueB
 *    
C
random_normal_6/stddevConst*
dtype0*
valueB
 *  ?

$random_normal_6/RandomStandardNormalRandomStandardNormalrandom_normal_6/shape*
dtype0*
seed2 *

seed *
T0
a
random_normal_6/mulMul$random_normal_6/RandomStandardNormalrandom_normal_6/stddev*
T0
J
random_normal_6Addrandom_normal_6/mulrandom_normal_6/mean*
T0
X

Variable_6Variable*
dtype0*
shape:@*
	container *
shared_name 
j
Variable_6/AssignAssign
Variable_6random_normal_6*
validate_shape(*
use_locking(*
T0
0
Variable_6/readIdentity
Variable_6*
T0
D
random_normal_7/shapeConst*
dtype0*
valueB:
A
random_normal_7/meanConst*
dtype0*
valueB
 *    
C
random_normal_7/stddevConst*
dtype0*
valueB
 *  ?

$random_normal_7/RandomStandardNormalRandomStandardNormalrandom_normal_7/shape*
dtype0*
seed2 *

seed *
T0
a
random_normal_7/mulMul$random_normal_7/RandomStandardNormalrandom_normal_7/stddev*
T0
J
random_normal_7Addrandom_normal_7/mulrandom_normal_7/mean*
T0
Y

Variable_7Variable*
dtype0*
shape:*
	container *
shared_name 
j
Variable_7/AssignAssign
Variable_7random_normal_7*
validate_shape(*
use_locking(*
T0
0
Variable_7/readIdentity
Variable_7*
T0
D
random_normal_8/shapeConst*
dtype0*
valueB:
A
random_normal_8/meanConst*
dtype0*
valueB
 *    
C
random_normal_8/stddevConst*
dtype0*
valueB
 *  ?

$random_normal_8/RandomStandardNormalRandomStandardNormalrandom_normal_8/shape*
dtype0*
seed2 *

seed *
T0
a
random_normal_8/mulMul$random_normal_8/RandomStandardNormalrandom_normal_8/stddev*
T0
J
random_normal_8Addrandom_normal_8/mulrandom_normal_8/mean*
T0
Y

Variable_8Variable*
dtype0*
shape:*
	container *
shared_name 
j
Variable_8/AssignAssign
Variable_8random_normal_8*
validate_shape(*
use_locking(*
T0
0
Variable_8/readIdentity
Variable_8*
T0
D
random_normal_9/shapeConst*
dtype0*
valueB:
A
random_normal_9/meanConst*
dtype0*
valueB
 *    
C
random_normal_9/stddevConst*
dtype0*
valueB
 *  ?

$random_normal_9/RandomStandardNormalRandomStandardNormalrandom_normal_9/shape*
dtype0*
seed2 *

seed *
T0
a
random_normal_9/mulMul$random_normal_9/RandomStandardNormalrandom_normal_9/stddev*
T0
J
random_normal_9Addrandom_normal_9/mulrandom_normal_9/mean*
T0
Y

Variable_9Variable*
dtype0*
shape:*
	container *
shared_name 
j
Variable_9/AssignAssign
Variable_9random_normal_9*
validate_shape(*
use_locking(*
T0
0
Variable_9/readIdentity
Variable_9*
T0
E
random_normal_10/shapeConst*
dtype0*
valueB:
B
random_normal_10/meanConst*
dtype0*
valueB
 *    
D
random_normal_10/stddevConst*
dtype0*
valueB
 *  ?

%random_normal_10/RandomStandardNormalRandomStandardNormalrandom_normal_10/shape*
dtype0*
seed2 *

seed *
T0
d
random_normal_10/mulMul%random_normal_10/RandomStandardNormalrandom_normal_10/stddev*
T0
M
random_normal_10Addrandom_normal_10/mulrandom_normal_10/mean*
T0
Z
Variable_10Variable*
dtype0*
shape:*
	container *
shared_name 
m
Variable_10/AssignAssignVariable_10random_normal_10*
validate_shape(*
use_locking(*
T0
2
Variable_10/readIdentityVariable_10*
T0
D
random_normal_11/shapeConst*
dtype0*
valueB:
B
random_normal_11/meanConst*
dtype0*
valueB
 *    
D
random_normal_11/stddevConst*
dtype0*
valueB
 *  ?

%random_normal_11/RandomStandardNormalRandomStandardNormalrandom_normal_11/shape*
dtype0*
seed2 *

seed *
T0
d
random_normal_11/mulMul%random_normal_11/RandomStandardNormalrandom_normal_11/stddev*
T0
M
random_normal_11Addrandom_normal_11/mulrandom_normal_11/mean*
T0
Y
Variable_11Variable*
dtype0*
shape:*
	container *
shared_name 
m
Variable_11/AssignAssignVariable_11random_normal_11*
validate_shape(*
use_locking(*
T0
2
Variable_11/readIdentityVariable_11*
T0
J
Reshape/shapeConst*
dtype0*%
valueB"’’’’         
5
ReshapeReshape	x_to_feedReshape/shape*
T0

Conv2DConv2DReshapeVariable/read*
paddingSAME*
strides
*
use_cudnn_on_gpu(*
data_formatNHWC*
T0
4
BiasAddBiasAddConv2DVariable_6/read*
T0

conv1ReluBiasAdd*
T0
i
pool1MaxPoolconv1*
paddingSAME*
strides
*
ksize
*
data_formatNHWC
U
norm1LRNpool1*
depth_radius*
alpha%Sé8*
beta%  @?*
bias%  ?
&
dropout/ShapeShapenorm1*
T0
G
dropout/random_uniform/minConst*
dtype0*
valueB
 *    
G
dropout/random_uniform/maxConst*
dtype0*
valueB
 *  ?
s
$dropout/random_uniform/RandomUniformRandomUniformdropout/Shape*
dtype0*
seed2 *

seed *
T0
b
dropout/random_uniform/subSubdropout/random_uniform/maxdropout/random_uniform/min*
T0
l
dropout/random_uniform/mulMul$dropout/random_uniform/RandomUniformdropout/random_uniform/sub*
T0
^
dropout/random_uniformAdddropout/random_uniform/muldropout/random_uniform/min*
T0
>
dropout/addAdd	keep_probdropout/random_uniform*
T0
,
dropout/FloorFloordropout/add*
T0
&
dropout/InvInv	keep_prob*
T0
/
dropout/mulMulnorm1dropout/Inv*
T0
9
dropout/mul_1Muldropout/muldropout/Floor*
T0

Conv2D_1Conv2Ddropout/mul_1Variable_1/read*
paddingSAME*
strides
*
use_cudnn_on_gpu(*
data_formatNHWC*
T0
8
	BiasAdd_1BiasAddConv2D_1Variable_7/read*
T0
!
conv2Relu	BiasAdd_1*
T0
i
pool2MaxPoolconv2*
paddingSAME*
strides
*
ksize
*
data_formatNHWC
U
norm2LRNpool2*
depth_radius*
alpha%Sé8*
beta%  @?*
bias%  ?
(
dropout_1/ShapeShapenorm2*
T0
I
dropout_1/random_uniform/minConst*
dtype0*
valueB
 *    
I
dropout_1/random_uniform/maxConst*
dtype0*
valueB
 *  ?
w
&dropout_1/random_uniform/RandomUniformRandomUniformdropout_1/Shape*
dtype0*
seed2 *

seed *
T0
h
dropout_1/random_uniform/subSubdropout_1/random_uniform/maxdropout_1/random_uniform/min*
T0
r
dropout_1/random_uniform/mulMul&dropout_1/random_uniform/RandomUniformdropout_1/random_uniform/sub*
T0
d
dropout_1/random_uniformAdddropout_1/random_uniform/muldropout_1/random_uniform/min*
T0
B
dropout_1/addAdd	keep_probdropout_1/random_uniform*
T0
0
dropout_1/FloorFloordropout_1/add*
T0
(
dropout_1/InvInv	keep_prob*
T0
3
dropout_1/mulMulnorm2dropout_1/Inv*
T0
?
dropout_1/mul_1Muldropout_1/muldropout_1/Floor*
T0

Conv2D_2Conv2Ddropout_1/mul_1Variable_2/read*
paddingSAME*
strides
*
use_cudnn_on_gpu(*
data_formatNHWC*
T0
8
	BiasAdd_2BiasAddConv2D_2Variable_8/read*
T0
!
conv3Relu	BiasAdd_2*
T0
i
pool3MaxPoolconv3*
paddingSAME*
strides
*
ksize
*
data_formatNHWC
U
norm3LRNpool3*
depth_radius*
alpha%Sé8*
beta%  @?*
bias%  ?
(
dropout_2/ShapeShapenorm3*
T0
I
dropout_2/random_uniform/minConst*
dtype0*
valueB
 *    
I
dropout_2/random_uniform/maxConst*
dtype0*
valueB
 *  ?
w
&dropout_2/random_uniform/RandomUniformRandomUniformdropout_2/Shape*
dtype0*
seed2 *

seed *
T0
h
dropout_2/random_uniform/subSubdropout_2/random_uniform/maxdropout_2/random_uniform/min*
T0
r
dropout_2/random_uniform/mulMul&dropout_2/random_uniform/RandomUniformdropout_2/random_uniform/sub*
T0
d
dropout_2/random_uniformAdddropout_2/random_uniform/muldropout_2/random_uniform/min*
T0
B
dropout_2/addAdd	keep_probdropout_2/random_uniform*
T0
0
dropout_2/FloorFloordropout_2/add*
T0
(
dropout_2/InvInv	keep_prob*
T0
3
dropout_2/mulMulnorm3dropout_2/Inv*
T0
?
dropout_2/mul_1Muldropout_2/muldropout_2/Floor*
T0
D
Reshape_1/shapeConst*
dtype0*
valueB"’’’’   
?
	Reshape_1Reshapedropout_2/mul_1Reshape_1/shape*
T0
[
MatMulMatMul	Reshape_1Variable_3/read*
transpose_b( *
transpose_a( *
T0
,
addAddMatMulVariable_9/read*
T0

fc1Reluadd*
T0
W
MatMul_1MatMulfc1Variable_4/read*
transpose_b( *
transpose_a( *
T0
1
add_1AddMatMul_1Variable_10/read*
T0

fc2Reluadd_1*
T0
W
MatMul_2MatMulfc2Variable_5/read*
transpose_b( *
transpose_a( *
T0
1
add_2AddMatMul_2Variable_11/read*
T0
#
kimenetIdentityadd_2*
T0
]
SoftmaxCrossEntropyWithLogitsSoftmaxCrossEntropyWithLogitskimenetPlaceholder*
T0
4
RankRankSoftmaxCrossEntropyWithLogits*
T0
5
range/startConst*
dtype0*
value	B : 
5
range/deltaConst*
dtype0*
value	B :
.
rangeRangerange/startRankrange/delta
L
MeanMeanSoftmaxCrossEntropyWithLogitsrange*
T0*
	keep_dims( 
'
gradients/ShapeShapeMean*
T0
<
gradients/ConstConst*
dtype0*
valueB
 *  ?
A
gradients/FillFillgradients/Shapegradients/Const*
T0
J
gradients/Mean_grad/ShapeShapeSoftmaxCrossEntropyWithLogits*
T0
H
gradients/Mean_grad/RankRankSoftmaxCrossEntropyWithLogits*
T0
4
gradients/Mean_grad/Shape_1Shaperange*
T0
I
gradients/Mean_grad/range/startConst*
dtype0*
value	B : 
I
gradients/Mean_grad/range/deltaConst*
dtype0*
value	B :
~
gradients/Mean_grad/rangeRangegradients/Mean_grad/range/startgradients/Mean_grad/Rankgradients/Mean_grad/range/delta
H
gradients/Mean_grad/Fill/valueConst*
dtype0*
value	B :
f
gradients/Mean_grad/FillFillgradients/Mean_grad/Shape_1gradients/Mean_grad/Fill/value*
T0

!gradients/Mean_grad/DynamicStitchDynamicStitchgradients/Mean_grad/rangerangegradients/Mean_grad/Shapegradients/Mean_grad/Fill*
T0*
N
j
gradients/Mean_grad/floordivDivgradients/Mean_grad/Shape!gradients/Mean_grad/DynamicStitch*
T0
b
gradients/Mean_grad/ReshapeReshapegradients/Fill!gradients/Mean_grad/DynamicStitch*
T0
d
gradients/Mean_grad/TileTilegradients/Mean_grad/Reshapegradients/Mean_grad/floordiv*
T0
L
gradients/Mean_grad/Shape_2ShapeSoftmaxCrossEntropyWithLogits*
T0
3
gradients/Mean_grad/Shape_3ShapeMean*
T0
H
gradients/Mean_grad/Rank_1Rankgradients/Mean_grad/Shape_2*
T0
K
!gradients/Mean_grad/range_1/startConst*
dtype0*
value	B : 
K
!gradients/Mean_grad/range_1/deltaConst*
dtype0*
value	B :

gradients/Mean_grad/range_1Range!gradients/Mean_grad/range_1/startgradients/Mean_grad/Rank_1!gradients/Mean_grad/range_1/delta
t
gradients/Mean_grad/ProdProdgradients/Mean_grad/Shape_2gradients/Mean_grad/range_1*
T0*
	keep_dims( 
H
gradients/Mean_grad/Rank_2Rankgradients/Mean_grad/Shape_3*
T0
K
!gradients/Mean_grad/range_2/startConst*
dtype0*
value	B : 
K
!gradients/Mean_grad/range_2/deltaConst*
dtype0*
value	B :

gradients/Mean_grad/range_2Range!gradients/Mean_grad/range_2/startgradients/Mean_grad/Rank_2!gradients/Mean_grad/range_2/delta
v
gradients/Mean_grad/Prod_1Prodgradients/Mean_grad/Shape_3gradients/Mean_grad/range_2*
T0*
	keep_dims( 
d
gradients/Mean_grad/floordiv_1Divgradients/Mean_grad/Prodgradients/Mean_grad/Prod_1*
T0
X
gradients/Mean_grad/CastCastgradients/Mean_grad/floordiv_1*

DstT0*

SrcT0
_
gradients/Mean_grad/truedivDivgradients/Mean_grad/Tilegradients/Mean_grad/Cast*
T0
U
gradients/zeros_like/ZerosLike	ZerosLikeSoftmaxCrossEntropyWithLogits:1*
T0
n
;gradients/SoftmaxCrossEntropyWithLogits_grad/ExpandDims/dimConst*
dtype0*
valueB :
’’’’’’’’’
Ø
7gradients/SoftmaxCrossEntropyWithLogits_grad/ExpandDims
ExpandDimsgradients/Mean_grad/truediv;gradients/SoftmaxCrossEntropyWithLogits_grad/ExpandDims/dim*
T0

0gradients/SoftmaxCrossEntropyWithLogits_grad/mulMul7gradients/SoftmaxCrossEntropyWithLogits_grad/ExpandDimsSoftmaxCrossEntropyWithLogits:1*
T0
6
gradients/add_2_grad/ShapeShapeMatMul_2*
T0
@
gradients/add_2_grad/Shape_1ShapeVariable_11/read*
T0
}
*gradients/add_2_grad/BroadcastGradientArgsBroadcastGradientArgsgradients/add_2_grad/Shapegradients/add_2_grad/Shape_1

gradients/add_2_grad/SumSum0gradients/SoftmaxCrossEntropyWithLogits_grad/mul*gradients/add_2_grad/BroadcastGradientArgs*
T0*
	keep_dims( 
f
gradients/add_2_grad/ReshapeReshapegradients/add_2_grad/Sumgradients/add_2_grad/Shape*
T0

gradients/add_2_grad/Sum_1Sum0gradients/SoftmaxCrossEntropyWithLogits_grad/mul,gradients/add_2_grad/BroadcastGradientArgs:1*
T0*
	keep_dims( 
l
gradients/add_2_grad/Reshape_1Reshapegradients/add_2_grad/Sum_1gradients/add_2_grad/Shape_1*
T0
m
%gradients/add_2_grad/tuple/group_depsNoOp^gradients/add_2_grad/Reshape^gradients/add_2_grad/Reshape_1

-gradients/add_2_grad/tuple/control_dependencyIdentitygradients/add_2_grad/Reshape&^gradients/add_2_grad/tuple/group_deps*
T0

/gradients/add_2_grad/tuple/control_dependency_1Identitygradients/add_2_grad/Reshape_1&^gradients/add_2_grad/tuple/group_deps*
T0

gradients/MatMul_2_grad/MatMulMatMul-gradients/add_2_grad/tuple/control_dependencyVariable_5/read*
transpose_b(*
transpose_a( *
T0

 gradients/MatMul_2_grad/MatMul_1MatMulfc2-gradients/add_2_grad/tuple/control_dependency*
transpose_b( *
transpose_a(*
T0
t
(gradients/MatMul_2_grad/tuple/group_depsNoOp^gradients/MatMul_2_grad/MatMul!^gradients/MatMul_2_grad/MatMul_1

0gradients/MatMul_2_grad/tuple/control_dependencyIdentitygradients/MatMul_2_grad/MatMul)^gradients/MatMul_2_grad/tuple/group_deps*
T0

2gradients/MatMul_2_grad/tuple/control_dependency_1Identity gradients/MatMul_2_grad/MatMul_1)^gradients/MatMul_2_grad/tuple/group_deps*
T0
g
gradients/fc2_grad/ReluGradReluGrad0gradients/MatMul_2_grad/tuple/control_dependencyfc2*
T0
6
gradients/add_1_grad/ShapeShapeMatMul_1*
T0
@
gradients/add_1_grad/Shape_1ShapeVariable_10/read*
T0
}
*gradients/add_1_grad/BroadcastGradientArgsBroadcastGradientArgsgradients/add_1_grad/Shapegradients/add_1_grad/Shape_1

gradients/add_1_grad/SumSumgradients/fc2_grad/ReluGrad*gradients/add_1_grad/BroadcastGradientArgs*
T0*
	keep_dims( 
f
gradients/add_1_grad/ReshapeReshapegradients/add_1_grad/Sumgradients/add_1_grad/Shape*
T0

gradients/add_1_grad/Sum_1Sumgradients/fc2_grad/ReluGrad,gradients/add_1_grad/BroadcastGradientArgs:1*
T0*
	keep_dims( 
l
gradients/add_1_grad/Reshape_1Reshapegradients/add_1_grad/Sum_1gradients/add_1_grad/Shape_1*
T0
m
%gradients/add_1_grad/tuple/group_depsNoOp^gradients/add_1_grad/Reshape^gradients/add_1_grad/Reshape_1

-gradients/add_1_grad/tuple/control_dependencyIdentitygradients/add_1_grad/Reshape&^gradients/add_1_grad/tuple/group_deps*
T0

/gradients/add_1_grad/tuple/control_dependency_1Identitygradients/add_1_grad/Reshape_1&^gradients/add_1_grad/tuple/group_deps*
T0

gradients/MatMul_1_grad/MatMulMatMul-gradients/add_1_grad/tuple/control_dependencyVariable_4/read*
transpose_b(*
transpose_a( *
T0

 gradients/MatMul_1_grad/MatMul_1MatMulfc1-gradients/add_1_grad/tuple/control_dependency*
transpose_b( *
transpose_a(*
T0
t
(gradients/MatMul_1_grad/tuple/group_depsNoOp^gradients/MatMul_1_grad/MatMul!^gradients/MatMul_1_grad/MatMul_1

0gradients/MatMul_1_grad/tuple/control_dependencyIdentitygradients/MatMul_1_grad/MatMul)^gradients/MatMul_1_grad/tuple/group_deps*
T0

2gradients/MatMul_1_grad/tuple/control_dependency_1Identity gradients/MatMul_1_grad/MatMul_1)^gradients/MatMul_1_grad/tuple/group_deps*
T0
g
gradients/fc1_grad/ReluGradReluGrad0gradients/MatMul_1_grad/tuple/control_dependencyfc1*
T0
2
gradients/add_grad/ShapeShapeMatMul*
T0
=
gradients/add_grad/Shape_1ShapeVariable_9/read*
T0
w
(gradients/add_grad/BroadcastGradientArgsBroadcastGradientArgsgradients/add_grad/Shapegradients/add_grad/Shape_1
~
gradients/add_grad/SumSumgradients/fc1_grad/ReluGrad(gradients/add_grad/BroadcastGradientArgs*
T0*
	keep_dims( 
`
gradients/add_grad/ReshapeReshapegradients/add_grad/Sumgradients/add_grad/Shape*
T0

gradients/add_grad/Sum_1Sumgradients/fc1_grad/ReluGrad*gradients/add_grad/BroadcastGradientArgs:1*
T0*
	keep_dims( 
f
gradients/add_grad/Reshape_1Reshapegradients/add_grad/Sum_1gradients/add_grad/Shape_1*
T0
g
#gradients/add_grad/tuple/group_depsNoOp^gradients/add_grad/Reshape^gradients/add_grad/Reshape_1

+gradients/add_grad/tuple/control_dependencyIdentitygradients/add_grad/Reshape$^gradients/add_grad/tuple/group_deps*
T0

-gradients/add_grad/tuple/control_dependency_1Identitygradients/add_grad/Reshape_1$^gradients/add_grad/tuple/group_deps*
T0

gradients/MatMul_grad/MatMulMatMul+gradients/add_grad/tuple/control_dependencyVariable_3/read*
transpose_b(*
transpose_a( *
T0

gradients/MatMul_grad/MatMul_1MatMul	Reshape_1+gradients/add_grad/tuple/control_dependency*
transpose_b( *
transpose_a(*
T0
n
&gradients/MatMul_grad/tuple/group_depsNoOp^gradients/MatMul_grad/MatMul^gradients/MatMul_grad/MatMul_1

.gradients/MatMul_grad/tuple/control_dependencyIdentitygradients/MatMul_grad/MatMul'^gradients/MatMul_grad/tuple/group_deps*
T0

0gradients/MatMul_grad/tuple/control_dependency_1Identitygradients/MatMul_grad/MatMul_1'^gradients/MatMul_grad/tuple/group_deps*
T0
A
gradients/Reshape_1_grad/ShapeShapedropout_2/mul_1*
T0

 gradients/Reshape_1_grad/ReshapeReshape.gradients/MatMul_grad/tuple/control_dependencygradients/Reshape_1_grad/Shape*
T0
E
$gradients/dropout_2/mul_1_grad/ShapeShapedropout_2/mul*
T0
I
&gradients/dropout_2/mul_1_grad/Shape_1Shapedropout_2/Floor*
T0

4gradients/dropout_2/mul_1_grad/BroadcastGradientArgsBroadcastGradientArgs$gradients/dropout_2/mul_1_grad/Shape&gradients/dropout_2/mul_1_grad/Shape_1
e
"gradients/dropout_2/mul_1_grad/mulMul gradients/Reshape_1_grad/Reshapedropout_2/Floor*
T0

"gradients/dropout_2/mul_1_grad/SumSum"gradients/dropout_2/mul_1_grad/mul4gradients/dropout_2/mul_1_grad/BroadcastGradientArgs*
T0*
	keep_dims( 

&gradients/dropout_2/mul_1_grad/ReshapeReshape"gradients/dropout_2/mul_1_grad/Sum$gradients/dropout_2/mul_1_grad/Shape*
T0
e
$gradients/dropout_2/mul_1_grad/mul_1Muldropout_2/mul gradients/Reshape_1_grad/Reshape*
T0
£
$gradients/dropout_2/mul_1_grad/Sum_1Sum$gradients/dropout_2/mul_1_grad/mul_16gradients/dropout_2/mul_1_grad/BroadcastGradientArgs:1*
T0*
	keep_dims( 

(gradients/dropout_2/mul_1_grad/Reshape_1Reshape$gradients/dropout_2/mul_1_grad/Sum_1&gradients/dropout_2/mul_1_grad/Shape_1*
T0

/gradients/dropout_2/mul_1_grad/tuple/group_depsNoOp'^gradients/dropout_2/mul_1_grad/Reshape)^gradients/dropout_2/mul_1_grad/Reshape_1
¦
7gradients/dropout_2/mul_1_grad/tuple/control_dependencyIdentity&gradients/dropout_2/mul_1_grad/Reshape0^gradients/dropout_2/mul_1_grad/tuple/group_deps*
T0
Ŗ
9gradients/dropout_2/mul_1_grad/tuple/control_dependency_1Identity(gradients/dropout_2/mul_1_grad/Reshape_10^gradients/dropout_2/mul_1_grad/tuple/group_deps*
T0
;
"gradients/dropout_2/mul_grad/ShapeShapenorm3*
T0
E
$gradients/dropout_2/mul_grad/Shape_1Shapedropout_2/Inv*
T0

2gradients/dropout_2/mul_grad/BroadcastGradientArgsBroadcastGradientArgs"gradients/dropout_2/mul_grad/Shape$gradients/dropout_2/mul_grad/Shape_1
x
 gradients/dropout_2/mul_grad/mulMul7gradients/dropout_2/mul_1_grad/tuple/control_dependencydropout_2/Inv*
T0

 gradients/dropout_2/mul_grad/SumSum gradients/dropout_2/mul_grad/mul2gradients/dropout_2/mul_grad/BroadcastGradientArgs*
T0*
	keep_dims( 
~
$gradients/dropout_2/mul_grad/ReshapeReshape gradients/dropout_2/mul_grad/Sum"gradients/dropout_2/mul_grad/Shape*
T0
r
"gradients/dropout_2/mul_grad/mul_1Mulnorm37gradients/dropout_2/mul_1_grad/tuple/control_dependency*
T0

"gradients/dropout_2/mul_grad/Sum_1Sum"gradients/dropout_2/mul_grad/mul_14gradients/dropout_2/mul_grad/BroadcastGradientArgs:1*
T0*
	keep_dims( 

&gradients/dropout_2/mul_grad/Reshape_1Reshape"gradients/dropout_2/mul_grad/Sum_1$gradients/dropout_2/mul_grad/Shape_1*
T0

-gradients/dropout_2/mul_grad/tuple/group_depsNoOp%^gradients/dropout_2/mul_grad/Reshape'^gradients/dropout_2/mul_grad/Reshape_1
 
5gradients/dropout_2/mul_grad/tuple/control_dependencyIdentity$gradients/dropout_2/mul_grad/Reshape.^gradients/dropout_2/mul_grad/tuple/group_deps*
T0
¤
7gradients/dropout_2/mul_grad/tuple/control_dependency_1Identity&gradients/dropout_2/mul_grad/Reshape_1.^gradients/dropout_2/mul_grad/tuple/group_deps*
T0
®
gradients/norm3_grad/LRNGradLRNGrad5gradients/dropout_2/mul_grad/tuple/control_dependencypool3norm3*
depth_radius*
alpha%Sé8*
beta%  @?*
bias%  ?
­
 gradients/pool3_grad/MaxPoolGradMaxPoolGradconv3pool3gradients/norm3_grad/LRNGrad*
paddingSAME*
strides
*
ksize
*
data_formatNHWC
[
gradients/conv3_grad/ReluGradReluGrad gradients/pool3_grad/MaxPoolGradconv3*
T0
M
gradients/BiasAdd_2_grad/RankRankgradients/conv3_grad/ReluGrad*
T0
H
gradients/BiasAdd_2_grad/sub/yConst*
dtype0*
value	B :
k
gradients/BiasAdd_2_grad/subSubgradients/BiasAdd_2_grad/Rankgradients/BiasAdd_2_grad/sub/y*
T0
N
$gradients/BiasAdd_2_grad/range/startConst*
dtype0*
value	B : 
N
$gradients/BiasAdd_2_grad/range/deltaConst*
dtype0*
value	B :

gradients/BiasAdd_2_grad/rangeRange$gradients/BiasAdd_2_grad/range/startgradients/BiasAdd_2_grad/sub$gradients/BiasAdd_2_grad/range/delta
|
gradients/BiasAdd_2_grad/SumSumgradients/conv3_grad/ReluGradgradients/BiasAdd_2_grad/range*
T0*
	keep_dims( 
p
)gradients/BiasAdd_2_grad/tuple/group_depsNoOp^gradients/conv3_grad/ReluGrad^gradients/BiasAdd_2_grad/Sum

1gradients/BiasAdd_2_grad/tuple/control_dependencyIdentitygradients/conv3_grad/ReluGrad*^gradients/BiasAdd_2_grad/tuple/group_deps*
T0

3gradients/BiasAdd_2_grad/tuple/control_dependency_1Identitygradients/BiasAdd_2_grad/Sum*^gradients/BiasAdd_2_grad/tuple/group_deps*
T0
@
gradients/Conv2D_2_grad/ShapeShapedropout_1/mul_1*
T0

+gradients/Conv2D_2_grad/Conv2DBackpropInputConv2DBackpropInputgradients/Conv2D_2_grad/ShapeVariable_2/read1gradients/BiasAdd_2_grad/tuple/control_dependency*
paddingSAME*
strides
*
use_cudnn_on_gpu(*
data_formatNHWC*
T0
B
gradients/Conv2D_2_grad/Shape_1ShapeVariable_2/read*
T0

,gradients/Conv2D_2_grad/Conv2DBackpropFilterConv2DBackpropFilterdropout_1/mul_1gradients/Conv2D_2_grad/Shape_11gradients/BiasAdd_2_grad/tuple/control_dependency*
paddingSAME*
strides
*
use_cudnn_on_gpu(*
data_formatNHWC*
T0

(gradients/Conv2D_2_grad/tuple/group_depsNoOp,^gradients/Conv2D_2_grad/Conv2DBackpropInput-^gradients/Conv2D_2_grad/Conv2DBackpropFilter

0gradients/Conv2D_2_grad/tuple/control_dependencyIdentity+gradients/Conv2D_2_grad/Conv2DBackpropInput)^gradients/Conv2D_2_grad/tuple/group_deps*
T0
 
2gradients/Conv2D_2_grad/tuple/control_dependency_1Identity,gradients/Conv2D_2_grad/Conv2DBackpropFilter)^gradients/Conv2D_2_grad/tuple/group_deps*
T0
E
$gradients/dropout_1/mul_1_grad/ShapeShapedropout_1/mul*
T0
I
&gradients/dropout_1/mul_1_grad/Shape_1Shapedropout_1/Floor*
T0

4gradients/dropout_1/mul_1_grad/BroadcastGradientArgsBroadcastGradientArgs$gradients/dropout_1/mul_1_grad/Shape&gradients/dropout_1/mul_1_grad/Shape_1
u
"gradients/dropout_1/mul_1_grad/mulMul0gradients/Conv2D_2_grad/tuple/control_dependencydropout_1/Floor*
T0

"gradients/dropout_1/mul_1_grad/SumSum"gradients/dropout_1/mul_1_grad/mul4gradients/dropout_1/mul_1_grad/BroadcastGradientArgs*
T0*
	keep_dims( 

&gradients/dropout_1/mul_1_grad/ReshapeReshape"gradients/dropout_1/mul_1_grad/Sum$gradients/dropout_1/mul_1_grad/Shape*
T0
u
$gradients/dropout_1/mul_1_grad/mul_1Muldropout_1/mul0gradients/Conv2D_2_grad/tuple/control_dependency*
T0
£
$gradients/dropout_1/mul_1_grad/Sum_1Sum$gradients/dropout_1/mul_1_grad/mul_16gradients/dropout_1/mul_1_grad/BroadcastGradientArgs:1*
T0*
	keep_dims( 

(gradients/dropout_1/mul_1_grad/Reshape_1Reshape$gradients/dropout_1/mul_1_grad/Sum_1&gradients/dropout_1/mul_1_grad/Shape_1*
T0

/gradients/dropout_1/mul_1_grad/tuple/group_depsNoOp'^gradients/dropout_1/mul_1_grad/Reshape)^gradients/dropout_1/mul_1_grad/Reshape_1
¦
7gradients/dropout_1/mul_1_grad/tuple/control_dependencyIdentity&gradients/dropout_1/mul_1_grad/Reshape0^gradients/dropout_1/mul_1_grad/tuple/group_deps*
T0
Ŗ
9gradients/dropout_1/mul_1_grad/tuple/control_dependency_1Identity(gradients/dropout_1/mul_1_grad/Reshape_10^gradients/dropout_1/mul_1_grad/tuple/group_deps*
T0
;
"gradients/dropout_1/mul_grad/ShapeShapenorm2*
T0
E
$gradients/dropout_1/mul_grad/Shape_1Shapedropout_1/Inv*
T0

2gradients/dropout_1/mul_grad/BroadcastGradientArgsBroadcastGradientArgs"gradients/dropout_1/mul_grad/Shape$gradients/dropout_1/mul_grad/Shape_1
x
 gradients/dropout_1/mul_grad/mulMul7gradients/dropout_1/mul_1_grad/tuple/control_dependencydropout_1/Inv*
T0

 gradients/dropout_1/mul_grad/SumSum gradients/dropout_1/mul_grad/mul2gradients/dropout_1/mul_grad/BroadcastGradientArgs*
T0*
	keep_dims( 
~
$gradients/dropout_1/mul_grad/ReshapeReshape gradients/dropout_1/mul_grad/Sum"gradients/dropout_1/mul_grad/Shape*
T0
r
"gradients/dropout_1/mul_grad/mul_1Mulnorm27gradients/dropout_1/mul_1_grad/tuple/control_dependency*
T0

"gradients/dropout_1/mul_grad/Sum_1Sum"gradients/dropout_1/mul_grad/mul_14gradients/dropout_1/mul_grad/BroadcastGradientArgs:1*
T0*
	keep_dims( 

&gradients/dropout_1/mul_grad/Reshape_1Reshape"gradients/dropout_1/mul_grad/Sum_1$gradients/dropout_1/mul_grad/Shape_1*
T0

-gradients/dropout_1/mul_grad/tuple/group_depsNoOp%^gradients/dropout_1/mul_grad/Reshape'^gradients/dropout_1/mul_grad/Reshape_1
 
5gradients/dropout_1/mul_grad/tuple/control_dependencyIdentity$gradients/dropout_1/mul_grad/Reshape.^gradients/dropout_1/mul_grad/tuple/group_deps*
T0
¤
7gradients/dropout_1/mul_grad/tuple/control_dependency_1Identity&gradients/dropout_1/mul_grad/Reshape_1.^gradients/dropout_1/mul_grad/tuple/group_deps*
T0
®
gradients/norm2_grad/LRNGradLRNGrad5gradients/dropout_1/mul_grad/tuple/control_dependencypool2norm2*
depth_radius*
alpha%Sé8*
beta%  @?*
bias%  ?
­
 gradients/pool2_grad/MaxPoolGradMaxPoolGradconv2pool2gradients/norm2_grad/LRNGrad*
paddingSAME*
strides
*
ksize
*
data_formatNHWC
[
gradients/conv2_grad/ReluGradReluGrad gradients/pool2_grad/MaxPoolGradconv2*
T0
M
gradients/BiasAdd_1_grad/RankRankgradients/conv2_grad/ReluGrad*
T0
H
gradients/BiasAdd_1_grad/sub/yConst*
dtype0*
value	B :
k
gradients/BiasAdd_1_grad/subSubgradients/BiasAdd_1_grad/Rankgradients/BiasAdd_1_grad/sub/y*
T0
N
$gradients/BiasAdd_1_grad/range/startConst*
dtype0*
value	B : 
N
$gradients/BiasAdd_1_grad/range/deltaConst*
dtype0*
value	B :

gradients/BiasAdd_1_grad/rangeRange$gradients/BiasAdd_1_grad/range/startgradients/BiasAdd_1_grad/sub$gradients/BiasAdd_1_grad/range/delta
|
gradients/BiasAdd_1_grad/SumSumgradients/conv2_grad/ReluGradgradients/BiasAdd_1_grad/range*
T0*
	keep_dims( 
p
)gradients/BiasAdd_1_grad/tuple/group_depsNoOp^gradients/conv2_grad/ReluGrad^gradients/BiasAdd_1_grad/Sum

1gradients/BiasAdd_1_grad/tuple/control_dependencyIdentitygradients/conv2_grad/ReluGrad*^gradients/BiasAdd_1_grad/tuple/group_deps*
T0

3gradients/BiasAdd_1_grad/tuple/control_dependency_1Identitygradients/BiasAdd_1_grad/Sum*^gradients/BiasAdd_1_grad/tuple/group_deps*
T0
>
gradients/Conv2D_1_grad/ShapeShapedropout/mul_1*
T0

+gradients/Conv2D_1_grad/Conv2DBackpropInputConv2DBackpropInputgradients/Conv2D_1_grad/ShapeVariable_1/read1gradients/BiasAdd_1_grad/tuple/control_dependency*
paddingSAME*
strides
*
use_cudnn_on_gpu(*
data_formatNHWC*
T0
B
gradients/Conv2D_1_grad/Shape_1ShapeVariable_1/read*
T0

,gradients/Conv2D_1_grad/Conv2DBackpropFilterConv2DBackpropFilterdropout/mul_1gradients/Conv2D_1_grad/Shape_11gradients/BiasAdd_1_grad/tuple/control_dependency*
paddingSAME*
strides
*
use_cudnn_on_gpu(*
data_formatNHWC*
T0

(gradients/Conv2D_1_grad/tuple/group_depsNoOp,^gradients/Conv2D_1_grad/Conv2DBackpropInput-^gradients/Conv2D_1_grad/Conv2DBackpropFilter

0gradients/Conv2D_1_grad/tuple/control_dependencyIdentity+gradients/Conv2D_1_grad/Conv2DBackpropInput)^gradients/Conv2D_1_grad/tuple/group_deps*
T0
 
2gradients/Conv2D_1_grad/tuple/control_dependency_1Identity,gradients/Conv2D_1_grad/Conv2DBackpropFilter)^gradients/Conv2D_1_grad/tuple/group_deps*
T0
A
"gradients/dropout/mul_1_grad/ShapeShapedropout/mul*
T0
E
$gradients/dropout/mul_1_grad/Shape_1Shapedropout/Floor*
T0

2gradients/dropout/mul_1_grad/BroadcastGradientArgsBroadcastGradientArgs"gradients/dropout/mul_1_grad/Shape$gradients/dropout/mul_1_grad/Shape_1
q
 gradients/dropout/mul_1_grad/mulMul0gradients/Conv2D_1_grad/tuple/control_dependencydropout/Floor*
T0

 gradients/dropout/mul_1_grad/SumSum gradients/dropout/mul_1_grad/mul2gradients/dropout/mul_1_grad/BroadcastGradientArgs*
T0*
	keep_dims( 
~
$gradients/dropout/mul_1_grad/ReshapeReshape gradients/dropout/mul_1_grad/Sum"gradients/dropout/mul_1_grad/Shape*
T0
q
"gradients/dropout/mul_1_grad/mul_1Muldropout/mul0gradients/Conv2D_1_grad/tuple/control_dependency*
T0

"gradients/dropout/mul_1_grad/Sum_1Sum"gradients/dropout/mul_1_grad/mul_14gradients/dropout/mul_1_grad/BroadcastGradientArgs:1*
T0*
	keep_dims( 

&gradients/dropout/mul_1_grad/Reshape_1Reshape"gradients/dropout/mul_1_grad/Sum_1$gradients/dropout/mul_1_grad/Shape_1*
T0

-gradients/dropout/mul_1_grad/tuple/group_depsNoOp%^gradients/dropout/mul_1_grad/Reshape'^gradients/dropout/mul_1_grad/Reshape_1
 
5gradients/dropout/mul_1_grad/tuple/control_dependencyIdentity$gradients/dropout/mul_1_grad/Reshape.^gradients/dropout/mul_1_grad/tuple/group_deps*
T0
¤
7gradients/dropout/mul_1_grad/tuple/control_dependency_1Identity&gradients/dropout/mul_1_grad/Reshape_1.^gradients/dropout/mul_1_grad/tuple/group_deps*
T0
9
 gradients/dropout/mul_grad/ShapeShapenorm1*
T0
A
"gradients/dropout/mul_grad/Shape_1Shapedropout/Inv*
T0

0gradients/dropout/mul_grad/BroadcastGradientArgsBroadcastGradientArgs gradients/dropout/mul_grad/Shape"gradients/dropout/mul_grad/Shape_1
r
gradients/dropout/mul_grad/mulMul5gradients/dropout/mul_1_grad/tuple/control_dependencydropout/Inv*
T0

gradients/dropout/mul_grad/SumSumgradients/dropout/mul_grad/mul0gradients/dropout/mul_grad/BroadcastGradientArgs*
T0*
	keep_dims( 
x
"gradients/dropout/mul_grad/ReshapeReshapegradients/dropout/mul_grad/Sum gradients/dropout/mul_grad/Shape*
T0
n
 gradients/dropout/mul_grad/mul_1Mulnorm15gradients/dropout/mul_1_grad/tuple/control_dependency*
T0

 gradients/dropout/mul_grad/Sum_1Sum gradients/dropout/mul_grad/mul_12gradients/dropout/mul_grad/BroadcastGradientArgs:1*
T0*
	keep_dims( 
~
$gradients/dropout/mul_grad/Reshape_1Reshape gradients/dropout/mul_grad/Sum_1"gradients/dropout/mul_grad/Shape_1*
T0

+gradients/dropout/mul_grad/tuple/group_depsNoOp#^gradients/dropout/mul_grad/Reshape%^gradients/dropout/mul_grad/Reshape_1

3gradients/dropout/mul_grad/tuple/control_dependencyIdentity"gradients/dropout/mul_grad/Reshape,^gradients/dropout/mul_grad/tuple/group_deps*
T0

5gradients/dropout/mul_grad/tuple/control_dependency_1Identity$gradients/dropout/mul_grad/Reshape_1,^gradients/dropout/mul_grad/tuple/group_deps*
T0
¬
gradients/norm1_grad/LRNGradLRNGrad3gradients/dropout/mul_grad/tuple/control_dependencypool1norm1*
depth_radius*
alpha%Sé8*
beta%  @?*
bias%  ?
­
 gradients/pool1_grad/MaxPoolGradMaxPoolGradconv1pool1gradients/norm1_grad/LRNGrad*
paddingSAME*
strides
*
ksize
*
data_formatNHWC
[
gradients/conv1_grad/ReluGradReluGrad gradients/pool1_grad/MaxPoolGradconv1*
T0
K
gradients/BiasAdd_grad/RankRankgradients/conv1_grad/ReluGrad*
T0
F
gradients/BiasAdd_grad/sub/yConst*
dtype0*
value	B :
e
gradients/BiasAdd_grad/subSubgradients/BiasAdd_grad/Rankgradients/BiasAdd_grad/sub/y*
T0
L
"gradients/BiasAdd_grad/range/startConst*
dtype0*
value	B : 
L
"gradients/BiasAdd_grad/range/deltaConst*
dtype0*
value	B :

gradients/BiasAdd_grad/rangeRange"gradients/BiasAdd_grad/range/startgradients/BiasAdd_grad/sub"gradients/BiasAdd_grad/range/delta
x
gradients/BiasAdd_grad/SumSumgradients/conv1_grad/ReluGradgradients/BiasAdd_grad/range*
T0*
	keep_dims( 
l
'gradients/BiasAdd_grad/tuple/group_depsNoOp^gradients/conv1_grad/ReluGrad^gradients/BiasAdd_grad/Sum

/gradients/BiasAdd_grad/tuple/control_dependencyIdentitygradients/conv1_grad/ReluGrad(^gradients/BiasAdd_grad/tuple/group_deps*
T0

1gradients/BiasAdd_grad/tuple/control_dependency_1Identitygradients/BiasAdd_grad/Sum(^gradients/BiasAdd_grad/tuple/group_deps*
T0
6
gradients/Conv2D_grad/ShapeShapeReshape*
T0
ż
)gradients/Conv2D_grad/Conv2DBackpropInputConv2DBackpropInputgradients/Conv2D_grad/ShapeVariable/read/gradients/BiasAdd_grad/tuple/control_dependency*
paddingSAME*
strides
*
use_cudnn_on_gpu(*
data_formatNHWC*
T0
>
gradients/Conv2D_grad/Shape_1ShapeVariable/read*
T0
ū
*gradients/Conv2D_grad/Conv2DBackpropFilterConv2DBackpropFilterReshapegradients/Conv2D_grad/Shape_1/gradients/BiasAdd_grad/tuple/control_dependency*
paddingSAME*
strides
*
use_cudnn_on_gpu(*
data_formatNHWC*
T0

&gradients/Conv2D_grad/tuple/group_depsNoOp*^gradients/Conv2D_grad/Conv2DBackpropInput+^gradients/Conv2D_grad/Conv2DBackpropFilter

.gradients/Conv2D_grad/tuple/control_dependencyIdentity)gradients/Conv2D_grad/Conv2DBackpropInput'^gradients/Conv2D_grad/tuple/group_deps*
T0

0gradients/Conv2D_grad/tuple/control_dependency_1Identity*gradients/Conv2D_grad/Conv2DBackpropFilter'^gradients/Conv2D_grad/tuple/group_deps*
T0
F
beta1_power/initial_valueConst*
dtype0*
valueB
 *fff?
U
beta1_powerVariable*
dtype0*
shape: *
	container *
shared_name 
v
beta1_power/AssignAssignbeta1_powerbeta1_power/initial_value*
validate_shape(*
use_locking(*
T0
2
beta1_power/readIdentitybeta1_power*
T0
F
beta2_power/initial_valueConst*
dtype0*
valueB
 *w¾?
U
beta2_powerVariable*
dtype0*
shape: *
	container *
shared_name 
v
beta2_power/AssignAssignbeta2_powerbeta2_power/initial_value*
validate_shape(*
use_locking(*
T0
2
beta2_power/readIdentitybeta2_power*
T0
B
zerosConst*
dtype0*%
valueB@*    
g
Variable/AdamVariable*
dtype0*
shape:@*
	container *
shared_name 
f
Variable/Adam/AssignAssignVariable/Adamzeros*
validate_shape(*
use_locking(*
T0
6
Variable/Adam/readIdentityVariable/Adam*
T0
D
zeros_1Const*
dtype0*%
valueB@*    
i
Variable/Adam_1Variable*
dtype0*
shape:@*
	container *
shared_name 
l
Variable/Adam_1/AssignAssignVariable/Adam_1zeros_1*
validate_shape(*
use_locking(*
T0
:
Variable/Adam_1/readIdentityVariable/Adam_1*
T0
E
zeros_2Const*
dtype0*&
valueB@*    
j
Variable_1/AdamVariable*
dtype0*
shape:@*
	container *
shared_name 
l
Variable_1/Adam/AssignAssignVariable_1/Adamzeros_2*
validate_shape(*
use_locking(*
T0
:
Variable_1/Adam/readIdentityVariable_1/Adam*
T0
E
zeros_3Const*
dtype0*&
valueB@*    
l
Variable_1/Adam_1Variable*
dtype0*
shape:@*
	container *
shared_name 
p
Variable_1/Adam_1/AssignAssignVariable_1/Adam_1zeros_3*
validate_shape(*
use_locking(*
T0
>
Variable_1/Adam_1/readIdentityVariable_1/Adam_1*
T0
F
zeros_4Const*
dtype0*'
valueB*    
k
Variable_2/AdamVariable*
dtype0*
shape:*
	container *
shared_name 
l
Variable_2/Adam/AssignAssignVariable_2/Adamzeros_4*
validate_shape(*
use_locking(*
T0
:
Variable_2/Adam/readIdentityVariable_2/Adam*
T0
F
zeros_5Const*
dtype0*'
valueB*    
m
Variable_2/Adam_1Variable*
dtype0*
shape:*
	container *
shared_name 
p
Variable_2/Adam_1/AssignAssignVariable_2/Adam_1zeros_5*
validate_shape(*
use_locking(*
T0
>
Variable_2/Adam_1/readIdentityVariable_2/Adam_1*
T0
>
zeros_6Const*
dtype0*
valueB
 *    
c
Variable_3/AdamVariable*
dtype0*
shape:
 *
	container *
shared_name 
l
Variable_3/Adam/AssignAssignVariable_3/Adamzeros_6*
validate_shape(*
use_locking(*
T0
:
Variable_3/Adam/readIdentityVariable_3/Adam*
T0
>
zeros_7Const*
dtype0*
valueB
 *    
e
Variable_3/Adam_1Variable*
dtype0*
shape:
 *
	container *
shared_name 
p
Variable_3/Adam_1/AssignAssignVariable_3/Adam_1zeros_7*
validate_shape(*
use_locking(*
T0
>
Variable_3/Adam_1/readIdentityVariable_3/Adam_1*
T0
>
zeros_8Const*
dtype0*
valueB
*    
c
Variable_4/AdamVariable*
dtype0*
shape:
*
	container *
shared_name 
l
Variable_4/Adam/AssignAssignVariable_4/Adamzeros_8*
validate_shape(*
use_locking(*
T0
:
Variable_4/Adam/readIdentityVariable_4/Adam*
T0
>
zeros_9Const*
dtype0*
valueB
*    
e
Variable_4/Adam_1Variable*
dtype0*
shape:
*
	container *
shared_name 
p
Variable_4/Adam_1/AssignAssignVariable_4/Adam_1zeros_9*
validate_shape(*
use_locking(*
T0
>
Variable_4/Adam_1/readIdentityVariable_4/Adam_1*
T0
>
zeros_10Const*
dtype0*
valueB	*    
b
Variable_5/AdamVariable*
dtype0*
shape:	*
	container *
shared_name 
m
Variable_5/Adam/AssignAssignVariable_5/Adamzeros_10*
validate_shape(*
use_locking(*
T0
:
Variable_5/Adam/readIdentityVariable_5/Adam*
T0
>
zeros_11Const*
dtype0*
valueB	*    
d
Variable_5/Adam_1Variable*
dtype0*
shape:	*
	container *
shared_name 
q
Variable_5/Adam_1/AssignAssignVariable_5/Adam_1zeros_11*
validate_shape(*
use_locking(*
T0
>
Variable_5/Adam_1/readIdentityVariable_5/Adam_1*
T0
9
zeros_12Const*
dtype0*
valueB@*    
]
Variable_6/AdamVariable*
dtype0*
shape:@*
	container *
shared_name 
m
Variable_6/Adam/AssignAssignVariable_6/Adamzeros_12*
validate_shape(*
use_locking(*
T0
:
Variable_6/Adam/readIdentityVariable_6/Adam*
T0
9
zeros_13Const*
dtype0*
valueB@*    
_
Variable_6/Adam_1Variable*
dtype0*
shape:@*
	container *
shared_name 
q
Variable_6/Adam_1/AssignAssignVariable_6/Adam_1zeros_13*
validate_shape(*
use_locking(*
T0
>
Variable_6/Adam_1/readIdentityVariable_6/Adam_1*
T0
:
zeros_14Const*
dtype0*
valueB*    
^
Variable_7/AdamVariable*
dtype0*
shape:*
	container *
shared_name 
m
Variable_7/Adam/AssignAssignVariable_7/Adamzeros_14*
validate_shape(*
use_locking(*
T0
:
Variable_7/Adam/readIdentityVariable_7/Adam*
T0
:
zeros_15Const*
dtype0*
valueB*    
`
Variable_7/Adam_1Variable*
dtype0*
shape:*
	container *
shared_name 
q
Variable_7/Adam_1/AssignAssignVariable_7/Adam_1zeros_15*
validate_shape(*
use_locking(*
T0
>
Variable_7/Adam_1/readIdentityVariable_7/Adam_1*
T0
:
zeros_16Const*
dtype0*
valueB*    
^
Variable_8/AdamVariable*
dtype0*
shape:*
	container *
shared_name 
m
Variable_8/Adam/AssignAssignVariable_8/Adamzeros_16*
validate_shape(*
use_locking(*
T0
:
Variable_8/Adam/readIdentityVariable_8/Adam*
T0
:
zeros_17Const*
dtype0*
valueB*    
`
Variable_8/Adam_1Variable*
dtype0*
shape:*
	container *
shared_name 
q
Variable_8/Adam_1/AssignAssignVariable_8/Adam_1zeros_17*
validate_shape(*
use_locking(*
T0
>
Variable_8/Adam_1/readIdentityVariable_8/Adam_1*
T0
:
zeros_18Const*
dtype0*
valueB*    
^
Variable_9/AdamVariable*
dtype0*
shape:*
	container *
shared_name 
m
Variable_9/Adam/AssignAssignVariable_9/Adamzeros_18*
validate_shape(*
use_locking(*
T0
:
Variable_9/Adam/readIdentityVariable_9/Adam*
T0
:
zeros_19Const*
dtype0*
valueB*    
`
Variable_9/Adam_1Variable*
dtype0*
shape:*
	container *
shared_name 
q
Variable_9/Adam_1/AssignAssignVariable_9/Adam_1zeros_19*
validate_shape(*
use_locking(*
T0
>
Variable_9/Adam_1/readIdentityVariable_9/Adam_1*
T0
:
zeros_20Const*
dtype0*
valueB*    
_
Variable_10/AdamVariable*
dtype0*
shape:*
	container *
shared_name 
o
Variable_10/Adam/AssignAssignVariable_10/Adamzeros_20*
validate_shape(*
use_locking(*
T0
<
Variable_10/Adam/readIdentityVariable_10/Adam*
T0
:
zeros_21Const*
dtype0*
valueB*    
a
Variable_10/Adam_1Variable*
dtype0*
shape:*
	container *
shared_name 
s
Variable_10/Adam_1/AssignAssignVariable_10/Adam_1zeros_21*
validate_shape(*
use_locking(*
T0
@
Variable_10/Adam_1/readIdentityVariable_10/Adam_1*
T0
9
zeros_22Const*
dtype0*
valueB*    
^
Variable_11/AdamVariable*
dtype0*
shape:*
	container *
shared_name 
o
Variable_11/Adam/AssignAssignVariable_11/Adamzeros_22*
validate_shape(*
use_locking(*
T0
<
Variable_11/Adam/readIdentityVariable_11/Adam*
T0
9
zeros_23Const*
dtype0*
valueB*    
`
Variable_11/Adam_1Variable*
dtype0*
shape:*
	container *
shared_name 
s
Variable_11/Adam_1/AssignAssignVariable_11/Adam_1zeros_23*
validate_shape(*
use_locking(*
T0
@
Variable_11/Adam_1/readIdentityVariable_11/Adam_1*
T0
?
Adam/learning_rateConst*
dtype0*
valueB
 *o:
7

Adam/beta1Const*
dtype0*
valueB
 *fff?
7

Adam/beta2Const*
dtype0*
valueB
 *w¾?
9
Adam/epsilonConst*
dtype0*
valueB
 *wĢ+2

Adam/update_Variable/ApplyAdam	ApplyAdamVariableVariable/AdamVariable/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon0gradients/Conv2D_grad/tuple/control_dependency_1*
use_locking( *
T0

 Adam/update_Variable_1/ApplyAdam	ApplyAdam
Variable_1Variable_1/AdamVariable_1/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon2gradients/Conv2D_1_grad/tuple/control_dependency_1*
use_locking( *
T0

 Adam/update_Variable_2/ApplyAdam	ApplyAdam
Variable_2Variable_2/AdamVariable_2/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon2gradients/Conv2D_2_grad/tuple/control_dependency_1*
use_locking( *
T0

 Adam/update_Variable_3/ApplyAdam	ApplyAdam
Variable_3Variable_3/AdamVariable_3/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon0gradients/MatMul_grad/tuple/control_dependency_1*
use_locking( *
T0

 Adam/update_Variable_4/ApplyAdam	ApplyAdam
Variable_4Variable_4/AdamVariable_4/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon2gradients/MatMul_1_grad/tuple/control_dependency_1*
use_locking( *
T0

 Adam/update_Variable_5/ApplyAdam	ApplyAdam
Variable_5Variable_5/AdamVariable_5/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon2gradients/MatMul_2_grad/tuple/control_dependency_1*
use_locking( *
T0

 Adam/update_Variable_6/ApplyAdam	ApplyAdam
Variable_6Variable_6/AdamVariable_6/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon1gradients/BiasAdd_grad/tuple/control_dependency_1*
use_locking( *
T0

 Adam/update_Variable_7/ApplyAdam	ApplyAdam
Variable_7Variable_7/AdamVariable_7/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon3gradients/BiasAdd_1_grad/tuple/control_dependency_1*
use_locking( *
T0

 Adam/update_Variable_8/ApplyAdam	ApplyAdam
Variable_8Variable_8/AdamVariable_8/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon3gradients/BiasAdd_2_grad/tuple/control_dependency_1*
use_locking( *
T0

 Adam/update_Variable_9/ApplyAdam	ApplyAdam
Variable_9Variable_9/AdamVariable_9/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon-gradients/add_grad/tuple/control_dependency_1*
use_locking( *
T0

!Adam/update_Variable_10/ApplyAdam	ApplyAdamVariable_10Variable_10/AdamVariable_10/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon/gradients/add_1_grad/tuple/control_dependency_1*
use_locking( *
T0

!Adam/update_Variable_11/ApplyAdam	ApplyAdamVariable_11Variable_11/AdamVariable_11/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon/gradients/add_2_grad/tuple/control_dependency_1*
use_locking( *
T0
Ś
Adam/mulMulbeta1_power/read
Adam/beta1^Adam/update_Variable/ApplyAdam!^Adam/update_Variable_1/ApplyAdam!^Adam/update_Variable_2/ApplyAdam!^Adam/update_Variable_3/ApplyAdam!^Adam/update_Variable_4/ApplyAdam!^Adam/update_Variable_5/ApplyAdam!^Adam/update_Variable_6/ApplyAdam!^Adam/update_Variable_7/ApplyAdam!^Adam/update_Variable_8/ApplyAdam!^Adam/update_Variable_9/ApplyAdam"^Adam/update_Variable_10/ApplyAdam"^Adam/update_Variable_11/ApplyAdam*
T0
^
Adam/AssignAssignbeta1_powerAdam/mul*
validate_shape(*
use_locking( *
T0
Ü

Adam/mul_1Mulbeta2_power/read
Adam/beta2^Adam/update_Variable/ApplyAdam!^Adam/update_Variable_1/ApplyAdam!^Adam/update_Variable_2/ApplyAdam!^Adam/update_Variable_3/ApplyAdam!^Adam/update_Variable_4/ApplyAdam!^Adam/update_Variable_5/ApplyAdam!^Adam/update_Variable_6/ApplyAdam!^Adam/update_Variable_7/ApplyAdam!^Adam/update_Variable_8/ApplyAdam!^Adam/update_Variable_9/ApplyAdam"^Adam/update_Variable_10/ApplyAdam"^Adam/update_Variable_11/ApplyAdam*
T0
b
Adam/Assign_1Assignbeta2_power
Adam/mul_1*
validate_shape(*
use_locking( *
T0
Ī
AdamNoOp^Adam/update_Variable/ApplyAdam!^Adam/update_Variable_1/ApplyAdam!^Adam/update_Variable_2/ApplyAdam!^Adam/update_Variable_3/ApplyAdam!^Adam/update_Variable_4/ApplyAdam!^Adam/update_Variable_5/ApplyAdam!^Adam/update_Variable_6/ApplyAdam!^Adam/update_Variable_7/ApplyAdam!^Adam/update_Variable_8/ApplyAdam!^Adam/update_Variable_9/ApplyAdam"^Adam/update_Variable_10/ApplyAdam"^Adam/update_Variable_11/ApplyAdam^Adam/Assign^Adam/Assign_1
:
ArgMax/dimensionConst*
dtype0*
value	B :
4
ArgMaxArgMaxkimenetArgMax/dimension*
T0
<
ArgMax_1/dimensionConst*
dtype0*
value	B :
<
ArgMax_1ArgMaxPlaceholderArgMax_1/dimension*
T0
)
EqualEqualArgMaxArgMax_1*
T0	
+
CastCastEqual*

DstT0*

SrcT0


Rank_1RankCast*
T0
7
range_1/startConst*
dtype0*
value	B : 
7
range_1/deltaConst*
dtype0*
value	B :
6
range_1Rangerange_1/startRank_1range_1/delta
7
Mean_1MeanCastrange_1*
T0*
	keep_dims( 

initNoOp^Variable/Assign^Variable_1/Assign^Variable_2/Assign^Variable_3/Assign^Variable_4/Assign^Variable_5/Assign^Variable_6/Assign^Variable_7/Assign^Variable_8/Assign^Variable_9/Assign^Variable_10/Assign^Variable_11/Assign^beta1_power/Assign^beta2_power/Assign^Variable/Adam/Assign^Variable/Adam_1/Assign^Variable_1/Adam/Assign^Variable_1/Adam_1/Assign^Variable_2/Adam/Assign^Variable_2/Adam_1/Assign^Variable_3/Adam/Assign^Variable_3/Adam_1/Assign^Variable_4/Adam/Assign^Variable_4/Adam_1/Assign^Variable_5/Adam/Assign^Variable_5/Adam_1/Assign^Variable_6/Adam/Assign^Variable_6/Adam_1/Assign^Variable_7/Adam/Assign^Variable_7/Adam_1/Assign^Variable_8/Adam/Assign^Variable_8/Adam_1/Assign^Variable_9/Adam/Assign^Variable_9/Adam_1/Assign^Variable_10/Adam/Assign^Variable_10/Adam_1/Assign^Variable_11/Adam/Assign^Variable_11/Adam_1/Assign
8

save/ConstConst*
dtype0*
valueB Bmodel

save/save/tensor_namesConst*
dtype0*ļ
valueåBā&BVariableBVariable/AdamBVariable/Adam_1B
Variable_1BVariable_1/AdamBVariable_1/Adam_1BVariable_10BVariable_10/AdamBVariable_10/Adam_1BVariable_11BVariable_11/AdamBVariable_11/Adam_1B
Variable_2BVariable_2/AdamBVariable_2/Adam_1B
Variable_3BVariable_3/AdamBVariable_3/Adam_1B
Variable_4BVariable_4/AdamBVariable_4/Adam_1B
Variable_5BVariable_5/AdamBVariable_5/Adam_1B
Variable_6BVariable_6/AdamBVariable_6/Adam_1B
Variable_7BVariable_7/AdamBVariable_7/Adam_1B
Variable_8BVariable_8/AdamBVariable_8/Adam_1B
Variable_9BVariable_9/AdamBVariable_9/Adam_1Bbeta1_powerBbeta2_power

save/save/shapes_and_slicesConst*
dtype0*_
valueVBT&B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 
ć
	save/save
SaveSlices
save/Constsave/save/tensor_namessave/save/shapes_and_slicesVariableVariable/AdamVariable/Adam_1
Variable_1Variable_1/AdamVariable_1/Adam_1Variable_10Variable_10/AdamVariable_10/Adam_1Variable_11Variable_11/AdamVariable_11/Adam_1
Variable_2Variable_2/AdamVariable_2/Adam_1
Variable_3Variable_3/AdamVariable_3/Adam_1
Variable_4Variable_4/AdamVariable_4/Adam_1
Variable_5Variable_5/AdamVariable_5/Adam_1
Variable_6Variable_6/AdamVariable_6/Adam_1
Variable_7Variable_7/AdamVariable_7/Adam_1
Variable_8Variable_8/AdamVariable_8/Adam_1
Variable_9Variable_9/AdamVariable_9/Adam_1beta1_powerbeta2_power*/
T*
(2&
D
save/control_dependencyIdentity
save/Const
^save/save*
T0
O
save/restore_slice/tensor_nameConst*
dtype0*
valueB BVariable
K
"save/restore_slice/shape_and_sliceConst*
dtype0*
valueB B 

save/restore_sliceRestoreSlice
save/Constsave/restore_slice/tensor_name"save/restore_slice/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
e
save/AssignAssignVariablesave/restore_slice*
validate_shape(*
use_locking(*
T0
V
 save/restore_slice_1/tensor_nameConst*
dtype0*
valueB BVariable/Adam
M
$save/restore_slice_1/shape_and_sliceConst*
dtype0*
valueB B 
¢
save/restore_slice_1RestoreSlice
save/Const save/restore_slice_1/tensor_name$save/restore_slice_1/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
n
save/Assign_1AssignVariable/Adamsave/restore_slice_1*
validate_shape(*
use_locking(*
T0
X
 save/restore_slice_2/tensor_nameConst*
dtype0* 
valueB BVariable/Adam_1
M
$save/restore_slice_2/shape_and_sliceConst*
dtype0*
valueB B 
¢
save/restore_slice_2RestoreSlice
save/Const save/restore_slice_2/tensor_name$save/restore_slice_2/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
p
save/Assign_2AssignVariable/Adam_1save/restore_slice_2*
validate_shape(*
use_locking(*
T0
S
 save/restore_slice_3/tensor_nameConst*
dtype0*
valueB B
Variable_1
M
$save/restore_slice_3/shape_and_sliceConst*
dtype0*
valueB B 
¢
save/restore_slice_3RestoreSlice
save/Const save/restore_slice_3/tensor_name$save/restore_slice_3/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
k
save/Assign_3Assign
Variable_1save/restore_slice_3*
validate_shape(*
use_locking(*
T0
X
 save/restore_slice_4/tensor_nameConst*
dtype0* 
valueB BVariable_1/Adam
M
$save/restore_slice_4/shape_and_sliceConst*
dtype0*
valueB B 
¢
save/restore_slice_4RestoreSlice
save/Const save/restore_slice_4/tensor_name$save/restore_slice_4/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
p
save/Assign_4AssignVariable_1/Adamsave/restore_slice_4*
validate_shape(*
use_locking(*
T0
Z
 save/restore_slice_5/tensor_nameConst*
dtype0*"
valueB BVariable_1/Adam_1
M
$save/restore_slice_5/shape_and_sliceConst*
dtype0*
valueB B 
¢
save/restore_slice_5RestoreSlice
save/Const save/restore_slice_5/tensor_name$save/restore_slice_5/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
r
save/Assign_5AssignVariable_1/Adam_1save/restore_slice_5*
validate_shape(*
use_locking(*
T0
T
 save/restore_slice_6/tensor_nameConst*
dtype0*
valueB BVariable_10
M
$save/restore_slice_6/shape_and_sliceConst*
dtype0*
valueB B 
¢
save/restore_slice_6RestoreSlice
save/Const save/restore_slice_6/tensor_name$save/restore_slice_6/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
l
save/Assign_6AssignVariable_10save/restore_slice_6*
validate_shape(*
use_locking(*
T0
Y
 save/restore_slice_7/tensor_nameConst*
dtype0*!
valueB BVariable_10/Adam
M
$save/restore_slice_7/shape_and_sliceConst*
dtype0*
valueB B 
¢
save/restore_slice_7RestoreSlice
save/Const save/restore_slice_7/tensor_name$save/restore_slice_7/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
q
save/Assign_7AssignVariable_10/Adamsave/restore_slice_7*
validate_shape(*
use_locking(*
T0
[
 save/restore_slice_8/tensor_nameConst*
dtype0*#
valueB BVariable_10/Adam_1
M
$save/restore_slice_8/shape_and_sliceConst*
dtype0*
valueB B 
¢
save/restore_slice_8RestoreSlice
save/Const save/restore_slice_8/tensor_name$save/restore_slice_8/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
s
save/Assign_8AssignVariable_10/Adam_1save/restore_slice_8*
validate_shape(*
use_locking(*
T0
T
 save/restore_slice_9/tensor_nameConst*
dtype0*
valueB BVariable_11
M
$save/restore_slice_9/shape_and_sliceConst*
dtype0*
valueB B 
¢
save/restore_slice_9RestoreSlice
save/Const save/restore_slice_9/tensor_name$save/restore_slice_9/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
l
save/Assign_9AssignVariable_11save/restore_slice_9*
validate_shape(*
use_locking(*
T0
Z
!save/restore_slice_10/tensor_nameConst*
dtype0*!
valueB BVariable_11/Adam
N
%save/restore_slice_10/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_10RestoreSlice
save/Const!save/restore_slice_10/tensor_name%save/restore_slice_10/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
s
save/Assign_10AssignVariable_11/Adamsave/restore_slice_10*
validate_shape(*
use_locking(*
T0
\
!save/restore_slice_11/tensor_nameConst*
dtype0*#
valueB BVariable_11/Adam_1
N
%save/restore_slice_11/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_11RestoreSlice
save/Const!save/restore_slice_11/tensor_name%save/restore_slice_11/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
u
save/Assign_11AssignVariable_11/Adam_1save/restore_slice_11*
validate_shape(*
use_locking(*
T0
T
!save/restore_slice_12/tensor_nameConst*
dtype0*
valueB B
Variable_2
N
%save/restore_slice_12/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_12RestoreSlice
save/Const!save/restore_slice_12/tensor_name%save/restore_slice_12/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
m
save/Assign_12Assign
Variable_2save/restore_slice_12*
validate_shape(*
use_locking(*
T0
Y
!save/restore_slice_13/tensor_nameConst*
dtype0* 
valueB BVariable_2/Adam
N
%save/restore_slice_13/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_13RestoreSlice
save/Const!save/restore_slice_13/tensor_name%save/restore_slice_13/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
r
save/Assign_13AssignVariable_2/Adamsave/restore_slice_13*
validate_shape(*
use_locking(*
T0
[
!save/restore_slice_14/tensor_nameConst*
dtype0*"
valueB BVariable_2/Adam_1
N
%save/restore_slice_14/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_14RestoreSlice
save/Const!save/restore_slice_14/tensor_name%save/restore_slice_14/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
t
save/Assign_14AssignVariable_2/Adam_1save/restore_slice_14*
validate_shape(*
use_locking(*
T0
T
!save/restore_slice_15/tensor_nameConst*
dtype0*
valueB B
Variable_3
N
%save/restore_slice_15/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_15RestoreSlice
save/Const!save/restore_slice_15/tensor_name%save/restore_slice_15/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
m
save/Assign_15Assign
Variable_3save/restore_slice_15*
validate_shape(*
use_locking(*
T0
Y
!save/restore_slice_16/tensor_nameConst*
dtype0* 
valueB BVariable_3/Adam
N
%save/restore_slice_16/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_16RestoreSlice
save/Const!save/restore_slice_16/tensor_name%save/restore_slice_16/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
r
save/Assign_16AssignVariable_3/Adamsave/restore_slice_16*
validate_shape(*
use_locking(*
T0
[
!save/restore_slice_17/tensor_nameConst*
dtype0*"
valueB BVariable_3/Adam_1
N
%save/restore_slice_17/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_17RestoreSlice
save/Const!save/restore_slice_17/tensor_name%save/restore_slice_17/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
t
save/Assign_17AssignVariable_3/Adam_1save/restore_slice_17*
validate_shape(*
use_locking(*
T0
T
!save/restore_slice_18/tensor_nameConst*
dtype0*
valueB B
Variable_4
N
%save/restore_slice_18/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_18RestoreSlice
save/Const!save/restore_slice_18/tensor_name%save/restore_slice_18/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
m
save/Assign_18Assign
Variable_4save/restore_slice_18*
validate_shape(*
use_locking(*
T0
Y
!save/restore_slice_19/tensor_nameConst*
dtype0* 
valueB BVariable_4/Adam
N
%save/restore_slice_19/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_19RestoreSlice
save/Const!save/restore_slice_19/tensor_name%save/restore_slice_19/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
r
save/Assign_19AssignVariable_4/Adamsave/restore_slice_19*
validate_shape(*
use_locking(*
T0
[
!save/restore_slice_20/tensor_nameConst*
dtype0*"
valueB BVariable_4/Adam_1
N
%save/restore_slice_20/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_20RestoreSlice
save/Const!save/restore_slice_20/tensor_name%save/restore_slice_20/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
t
save/Assign_20AssignVariable_4/Adam_1save/restore_slice_20*
validate_shape(*
use_locking(*
T0
T
!save/restore_slice_21/tensor_nameConst*
dtype0*
valueB B
Variable_5
N
%save/restore_slice_21/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_21RestoreSlice
save/Const!save/restore_slice_21/tensor_name%save/restore_slice_21/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
m
save/Assign_21Assign
Variable_5save/restore_slice_21*
validate_shape(*
use_locking(*
T0
Y
!save/restore_slice_22/tensor_nameConst*
dtype0* 
valueB BVariable_5/Adam
N
%save/restore_slice_22/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_22RestoreSlice
save/Const!save/restore_slice_22/tensor_name%save/restore_slice_22/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
r
save/Assign_22AssignVariable_5/Adamsave/restore_slice_22*
validate_shape(*
use_locking(*
T0
[
!save/restore_slice_23/tensor_nameConst*
dtype0*"
valueB BVariable_5/Adam_1
N
%save/restore_slice_23/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_23RestoreSlice
save/Const!save/restore_slice_23/tensor_name%save/restore_slice_23/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
t
save/Assign_23AssignVariable_5/Adam_1save/restore_slice_23*
validate_shape(*
use_locking(*
T0
T
!save/restore_slice_24/tensor_nameConst*
dtype0*
valueB B
Variable_6
N
%save/restore_slice_24/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_24RestoreSlice
save/Const!save/restore_slice_24/tensor_name%save/restore_slice_24/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
m
save/Assign_24Assign
Variable_6save/restore_slice_24*
validate_shape(*
use_locking(*
T0
Y
!save/restore_slice_25/tensor_nameConst*
dtype0* 
valueB BVariable_6/Adam
N
%save/restore_slice_25/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_25RestoreSlice
save/Const!save/restore_slice_25/tensor_name%save/restore_slice_25/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
r
save/Assign_25AssignVariable_6/Adamsave/restore_slice_25*
validate_shape(*
use_locking(*
T0
[
!save/restore_slice_26/tensor_nameConst*
dtype0*"
valueB BVariable_6/Adam_1
N
%save/restore_slice_26/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_26RestoreSlice
save/Const!save/restore_slice_26/tensor_name%save/restore_slice_26/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
t
save/Assign_26AssignVariable_6/Adam_1save/restore_slice_26*
validate_shape(*
use_locking(*
T0
T
!save/restore_slice_27/tensor_nameConst*
dtype0*
valueB B
Variable_7
N
%save/restore_slice_27/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_27RestoreSlice
save/Const!save/restore_slice_27/tensor_name%save/restore_slice_27/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
m
save/Assign_27Assign
Variable_7save/restore_slice_27*
validate_shape(*
use_locking(*
T0
Y
!save/restore_slice_28/tensor_nameConst*
dtype0* 
valueB BVariable_7/Adam
N
%save/restore_slice_28/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_28RestoreSlice
save/Const!save/restore_slice_28/tensor_name%save/restore_slice_28/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
r
save/Assign_28AssignVariable_7/Adamsave/restore_slice_28*
validate_shape(*
use_locking(*
T0
[
!save/restore_slice_29/tensor_nameConst*
dtype0*"
valueB BVariable_7/Adam_1
N
%save/restore_slice_29/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_29RestoreSlice
save/Const!save/restore_slice_29/tensor_name%save/restore_slice_29/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
t
save/Assign_29AssignVariable_7/Adam_1save/restore_slice_29*
validate_shape(*
use_locking(*
T0
T
!save/restore_slice_30/tensor_nameConst*
dtype0*
valueB B
Variable_8
N
%save/restore_slice_30/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_30RestoreSlice
save/Const!save/restore_slice_30/tensor_name%save/restore_slice_30/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
m
save/Assign_30Assign
Variable_8save/restore_slice_30*
validate_shape(*
use_locking(*
T0
Y
!save/restore_slice_31/tensor_nameConst*
dtype0* 
valueB BVariable_8/Adam
N
%save/restore_slice_31/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_31RestoreSlice
save/Const!save/restore_slice_31/tensor_name%save/restore_slice_31/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
r
save/Assign_31AssignVariable_8/Adamsave/restore_slice_31*
validate_shape(*
use_locking(*
T0
[
!save/restore_slice_32/tensor_nameConst*
dtype0*"
valueB BVariable_8/Adam_1
N
%save/restore_slice_32/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_32RestoreSlice
save/Const!save/restore_slice_32/tensor_name%save/restore_slice_32/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
t
save/Assign_32AssignVariable_8/Adam_1save/restore_slice_32*
validate_shape(*
use_locking(*
T0
T
!save/restore_slice_33/tensor_nameConst*
dtype0*
valueB B
Variable_9
N
%save/restore_slice_33/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_33RestoreSlice
save/Const!save/restore_slice_33/tensor_name%save/restore_slice_33/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
m
save/Assign_33Assign
Variable_9save/restore_slice_33*
validate_shape(*
use_locking(*
T0
Y
!save/restore_slice_34/tensor_nameConst*
dtype0* 
valueB BVariable_9/Adam
N
%save/restore_slice_34/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_34RestoreSlice
save/Const!save/restore_slice_34/tensor_name%save/restore_slice_34/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
r
save/Assign_34AssignVariable_9/Adamsave/restore_slice_34*
validate_shape(*
use_locking(*
T0
[
!save/restore_slice_35/tensor_nameConst*
dtype0*"
valueB BVariable_9/Adam_1
N
%save/restore_slice_35/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_35RestoreSlice
save/Const!save/restore_slice_35/tensor_name%save/restore_slice_35/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
t
save/Assign_35AssignVariable_9/Adam_1save/restore_slice_35*
validate_shape(*
use_locking(*
T0
U
!save/restore_slice_36/tensor_nameConst*
dtype0*
valueB Bbeta1_power
N
%save/restore_slice_36/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_36RestoreSlice
save/Const!save/restore_slice_36/tensor_name%save/restore_slice_36/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
n
save/Assign_36Assignbeta1_powersave/restore_slice_36*
validate_shape(*
use_locking(*
T0
U
!save/restore_slice_37/tensor_nameConst*
dtype0*
valueB Bbeta2_power
N
%save/restore_slice_37/shape_and_sliceConst*
dtype0*
valueB B 
„
save/restore_slice_37RestoreSlice
save/Const!save/restore_slice_37/tensor_name%save/restore_slice_37/shape_and_slice*
preferred_shard’’’’’’’’’*
dt0
n
save/Assign_37Assignbeta2_powersave/restore_slice_37*
validate_shape(*
use_locking(*
T0

save/restore_allNoOp^save/Assign^save/Assign_1^save/Assign_2^save/Assign_3^save/Assign_4^save/Assign_5^save/Assign_6^save/Assign_7^save/Assign_8^save/Assign_9^save/Assign_10^save/Assign_11^save/Assign_12^save/Assign_13^save/Assign_14^save/Assign_15^save/Assign_16^save/Assign_17^save/Assign_18^save/Assign_19^save/Assign_20^save/Assign_21^save/Assign_22^save/Assign_23^save/Assign_24^save/Assign_25^save/Assign_26^save/Assign_27^save/Assign_28^save/Assign_29^save/Assign_30^save/Assign_31^save/Assign_32^save/Assign_33^save/Assign_34^save/Assign_35^save/Assign_36^save/Assign_37"	