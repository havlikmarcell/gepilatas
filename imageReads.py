import simpleCV_processing
import SimpleCV
from os.path import isfile, join

import os

n_classes = 10

def listOneHotEncoder(li) :
    theList = list()
    for v in li:
        theList.append(oneHotEncoder(v))
    return theList

def oneHotEncoder( v) :
    global n_classes
    return [0]*v + [1] + [0]*(n_classes-v-1)


def readDirImages(myPath, v) :
    imData_x = list()
    imData_y = list()
    onlyfiles = [f for f in os.listdir(myPath) if isfile(join(myPath, f))]
    for fileName in onlyfiles:
        imData_x.append(simpleCV_processing.imageprepare(myPath + "/" + fileName))
        imData_y.append(oneHotEncoder(v))
    return imData_x, imData_y