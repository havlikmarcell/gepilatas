from os.path import isfile, join
from alexnet import alex_net
import tensorflow as tf
import myDataSet
import simpleCV_processing
import SimpleCV
import numpy as np
import os
from imageReads import readDirImages, listOneHotEncoder
import imageReads
import time
import tensorflow as tf

n_classes = 10
# some constants
backgroundNum = 3 # hatter
stop = 0
roundAbout= 1
giveWay = 2
speed30 = 4
speed60 = 5
speed100 = 6

imData_x_val = list()
imData_y_val = list()
myPath = "Images/validation"
onlyfiles = ['01349_00000.ppm', '00168_00002.ppm', '00916_00000.ppm', '01368_00001.ppm', '00002_00027.ppm', '00000_00024.ppm', '01038_00001.ppm']
labels = listOneHotEncoder([1,1,2,2,0,0,2])#[[0, 1, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 1, 0], [1, 0, 0, 0], [1, 0, 0, 0], [0, 0, 1, 0]]
for i in range(len(onlyfiles) ) :
    imData_x_val.append(simpleCV_processing.imageprepare(myPath + "/" + onlyfiles[i]))
    imData_y_val.append(labels[i])

myPath = "Images/validationBackground"
onlyfiles = [f for f in os.listdir(myPath) if isfile(join(myPath, f))]
backgroundPicNumVal = 0
# for fileName in onlyfiles:
#     im = SimpleCV.Image(myPath + "/" + fileName)
#     boxList = simpleCV_processing.getImageSliceBoxes(im, 28, 28, 8)
#     myList = simpleCV_processing.getImageSlices(im, boxList)
#     imData_x_val += myList
#     for i in range(len(boxList)):
#         imData_y_val.append(imageReads.oneHotEncoder(backgroundNum))
#     backgroundPicNumVal += len(boxList)

# Import image data

# Network Parameters
n_input = 784 # MNIST data input (img shape: 28*28)l
dropout = 0.90 # Dropout, probability to keep units

# tf Graph input
inChannels = 3
x = tf.placeholder(tf.float32, [None, n_input, inChannels], name="x_to_feed")
y = tf.placeholder(tf.float32, [None, n_classes])
keep_prob = tf.placeholder(tf.float32, name="keep_prob") # dropout (keep probability)

imData_x = list()
imData_y = list()
myPath = "Images/stopSign"
onlyfiles = [f for f in os.listdir(myPath) if isfile(join(myPath, f))]
for fileName in onlyfiles:
    imData_x.append(simpleCV_processing.imageprepare(myPath + "/" + fileName))
    imData_y.append(imageReads.oneHotEncoder(stop))

myPath = "Images/roundAbout"
onlyfiles = [f for f in os.listdir(myPath) if isfile(join(myPath, f))]
for fileName in onlyfiles:
    imData_x.append(simpleCV_processing.imageprepare(myPath + "/" + fileName))
    imData_y.append(imageReads.oneHotEncoder(roundAbout))
myPath = "Images/giveWay"
onlyfiles = [f for f in os.listdir(myPath) if isfile(join(myPath, f))]
for fileName in onlyfiles:
    imData_x.append(simpleCV_processing.imageprepare(myPath + "/" + fileName))
    imData_y.append(imageReads.oneHotEncoder(giveWay))
myPath = "Images/30"
onlyfiles = [f for f in os.listdir(myPath) if isfile(join(myPath, f))]
for fileName in onlyfiles:
    imData_x.append(simpleCV_processing.imageprepare(myPath + "/" + fileName))
    imData_y.append(imageReads.oneHotEncoder(speed30))
myPath = "Images/60"
onlyfiles = [f for f in os.listdir(myPath) if isfile(join(myPath, f))]
for fileName in onlyfiles:
    imData_x.append(simpleCV_processing.imageprepare(myPath + "/" + fileName))
    imData_y.append(imageReads.oneHotEncoder(speed60))
myPath = "Images/100"
onlyfiles = [f for f in os.listdir(myPath) if isfile(join(myPath, f))]
for fileName in onlyfiles:
    imData_x.append(simpleCV_processing.imageprepare(myPath + "/" + fileName))
    imData_y.append(imageReads.oneHotEncoder(speed100))

imData_x = imData_x * 30
imData_y = imData_y * 30
myPath = "Images/ourNothing"

print "Already ", len(imData_x)
myPath = "Images/background"
onlyfiles = [f for f in os.listdir(myPath) if isfile(join(myPath, f))]
backgroundPicNum = 0
for fileName in onlyfiles :
    im = SimpleCV.Image(myPath + "/" + fileName)
    boxList = simpleCV_processing.getImageSliceBoxes(im, 28, 28, 8)
    myList = simpleCV_processing.getImageSlices(im, boxList)
    imData_x += myList
    for i in range(len(boxList)):
        imData_y.append(imageReads.oneHotEncoder(backgroundNum))
    backgroundPicNum += len(boxList)
    print backgroundPicNum
    if backgroundPicNum>20000:
        break
myPath = "Images/ourBackgrounds"
onlyfiles = [f for f in os.listdir(myPath) if isfile(join(myPath, f))]
backgroundPicNum = 0
for fileName in onlyfiles :
    im = SimpleCV.Image(myPath + "/" + fileName)
    im = simpleCV_processing.imgResize(im, 640)
    boxList = simpleCV_processing.getImageSliceBoxes(im, 100, 100, 10)
    myList = simpleCV_processing.getImageSlices(im, boxList)
    imData_x += myList
    for i in range(len(boxList)):
        imData_y.append(imageReads.oneHotEncoder(backgroundNum))
    backgroundPicNum += len(boxList)
    print backgroundPicNum

trainDataSet = myDataSet.MyDataSet(np.concatenate(imData_x).reshape(len(imData_x), 784,3), np.array(imData_y) )

# Construct model
pred = alex_net(x, keep_prob)

# Parameters
learning_rate = 0.001
display_step = 1
# Define loss and optimizer
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(pred, y))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate, beta1=0.95, beta2=0.995).minimize(cost)

# Evaluate model
correct_pred = tf.equal(tf.argmax(pred,1), tf.argmax(y,1))
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))


print "Number of pictures: ", len(imData_x)
# Launch the graph
with tf.Session() as sess:
    # Initializing the variables
    sess.run(tf.initialize_all_variables())
    saver = tf.train.Saver()
    restoreLastModel = True
    if restoreLastModel :
        saver.restore(sess, "model/model.ckpt")
        pass
    step = 1
    batch_size = 256*3
    training_iters = 180
    # Keep training until reach max iterations
    while step < training_iters:
        timer = time.time()
        batch_xs, batch_ys = trainDataSet.next_batch(batch_size)
        # Fit training using batch data
        _, trainAcc, trainLoss = sess.run([optimizer, accuracy, cost], feed_dict={x: batch_xs, y: batch_ys, keep_prob: dropout})
        if step % display_step == 0:
            # Calculate validation accuracy and loss
            acc, loss = sess.run([accuracy, cost] , feed_dict={x: imData_x_val, y: imData_y_val, keep_prob: 1.})
            
            print "{:3} Iter {:05}, Tloss: {:.3}, TAcc: {:.3}, Vloss: {:.3}, VAcc: {:.3} Still: {:3}".format(step, step*batch_size, trainLoss, trainAcc, loss, acc, (time.time() - timer)*(training_iters-step))
        step += 1
    print "Optimization Finished!"
    tf.train.write_graph(sess.graph.as_graph_def(), os.path.dirname(os.path.realpath(__file__)), 'model/model.pb', as_text=False)
    
    save_path = saver.save(sess, "model/model.ckpt")
    print "Model saved in file: ", save_path