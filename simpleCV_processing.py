import SimpleCV as SCV
import os



def imageprepare(filePath):
    runDir = os.path.dirname(os.path.realpath(__file__))
    fileFullPath = os.path.join(runDir, filePath)
    im = SCV.Image(fileFullPath)

    return image_preprocess_and_resize_to_numpy(im)

def image_preprocess_and_resize_to_numpy(img):
    img = img.resize(28, 28)
    numpyData = (255 - img.getNumpy().reshape(784, 3))/255.
    return numpyData


def imgResize(img, w):

    return img.resize(w, img.height * w / img.width)
def getImageSlices(img, boxList):

    myList = list()
    for box in boxList:
        myList.append(img.crop(box[0][0], box[0][1], box[1][0], box[1][1]))

    resizedList = list()
    for i in myList :
        imgResized = i.resize(28,28)
        npArray = (255 - imgResized.getNumpy().reshape(784,3))/255.
        resizedList.append(npArray)


    return resizedList

def getImageSliceBoxes(img, width, height, step):
    boxList = list()
    for x in range(0, img.width - width, step):
        for y in range(0, img.height - height, step):
            boxList.append([x, y, width, height])

    remadeBoxes = list()
    for b in boxList:
        remadeBoxes.append([(b[0], b[1]), (b[2], b[3])])

    return remadeBoxes