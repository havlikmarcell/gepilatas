import tensorflow as tf
import os
from os.path import isfile, join
import image_preprocess
from alexnet import alex_net
from PIL import ImageDraw
import time
from SimpleCV import Camera, DrawingLayer
import SimpleCV
from simpleCV_processing import  getImageSlices, getImageSliceBoxes

# Initialize the camera
cam = Camera()

# Network Parameters
n_input = 784  # MNIST data input (img shape: 28*28)
batch_size = 1
dropout = 0.8  # Dropout, probability to keep units

n_classes = 4  # MNIST total classes (0-9 digits)
inChannels = 3  # Szin csatornak szama
# tf Graph input
x = tf.placeholder(tf.float32, [None, n_input, inChannels], name="x_to_feed")
y = tf.placeholder(tf.float32, [None, n_classes])
keep_prob = tf.placeholder(tf.float32, name="keep_prob")  # dropout (keep probability)

# Construct model
pred = alex_net(x, keep_prob)

# Initializing the variables
init = tf.initialize_all_variables()
res = tf.argmax(pred, 1)

with tf.Session() as sess:
    sess.run(tf.initialize_all_variables())

    saver = tf.train.Saver()
    saver.restore(sess, "model/model.ckpt")
    print ("Model restored.")
    colors = [(255, 0, 0, 200), (0, 0, 255, 200), (0, 255, 0, 200), (0, 0, 0, 200), (255, 255, 0, 200), (255, 255, 0, 200), (255, 255, 0, 200)]
    img = cam.getImage()
    remadeBoxList = getImageSliceBoxes(img, 100, 100, 20)
    # remadeBoxList += getImageSliceBoxes(img, 150, 150, 30)
    #display = SimpleCV.Display((1400, 900))

    while 1:

        # Get Image from camera
        img = cam.getImage()
        #img = image_preprocess(img)
        imgList = getImageSlices(img, remadeBoxList)

        #imgData = img.resize(28,28).getNumpy().reshape(1,784,3)


        timer = time.time()
        result = sess.run(res, feed_dict={x: imgList, keep_prob: 1.0})
        timer2 = time.time()

        facelayer = DrawingLayer((img.width, img.height))
        for c, box in enumerate(remadeBoxList) :
            if result[c] != 3:
                facebox = facelayer.rectangle(box[0], box[1], colors[result[c]], width=3)
        print ("Predict time: {:.4} Image edit time: {:.4}".format(timer2 - timer, time.time() - timer2))

        img.addDrawingLayer(facelayer)
        img.applyLayers()

        img.show()

#        draw.rectangle(boxList[p][i], outline=colors[result[i]])

        # draw = ImageDraw.Draw(imList[p])
        # for i in range(len(boxList[p])):
        #     if result[i] != 3:
        #         draw.rectangle(boxList[p][i], outline=colors[result[i]])
        # imList[p].show()












