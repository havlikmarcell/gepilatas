import pandas as pd

class MyPandaDataSet(object):

    def __init__(self, data):
        self._data = data
        
    def get_data(self):   
        feature = self._data.drop( 'target', axis = 1 )
        target = self._data.target.values
        return feature, target

    @property
    def num_examples(self):
        return len(self._target)

    