from PIL import Image, ImageFilter
import os
imageSize_x = 28
imageSize_y = 28

def imageResizeWidth(im, width):
    orig_w = im.size[0]
    orig_h = im.size[1]
    return im.resize((width, width * orig_h/orig_w), Image.ANTIALIAS).filter(ImageFilter.SHARPEN)
def imageResize(im):
    newImage = Image.new('RGB', (imageSize_x, imageSize_y), (255,255,255)) #creates white canvas of 28x28 pixels
    #newImage = Image.new('L', (imageSize_x, imageSize_y), (255)) #creates white canvas of 28x28 pixels
    img = im.resize((imageSize_x, imageSize_y), Image.ANTIALIAS).filter(ImageFilter.SHARPEN)
    newImage.paste(img, (0, 0)) #paste resized image on white canvas
    tv = list(newImage.getdata()) #get pixel values
    
    #normalize pixels to 0 and 1. 0 is pure white, 1 is pure black.
    tva = list()    
    for x in tv :
        tva.append( [ (255-c)*1.0/255.0 for c in x] ) 
    #newImage.show()
    return tva

def imageCrop(im, box):
    return imageResize(im.crop(box))

def imageprepare(filePath):
    runDir = os.path.dirname(os.path.realpath(__file__))
    fileFullPath = os.path.join(runDir, filePath)
    im = Image.open(fileFullPath)
    
    return imageResize(im)
def getImage(filePath):
    runDir = os.path.dirname(os.path.realpath(__file__))
    fileFullPath = os.path.join(runDir, filePath)
    im = Image.open(fileFullPath)
    return im
def getManyCropSample(im, width, height, step):
    boxList = list()
    for x in range(0, im.size[0]-width, step) :
        for y in range(0, im.size[1]-height, step) :
            boxList.append([x, y, x + width, y + height])
    myList = list()
    for box in boxList:
        myList.append(imageCrop(im, box))
    return myList, boxList

